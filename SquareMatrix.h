#pragma once

class SquareMatrix
{
	//�� ��������� ���� ������ ���������, ��� ��� private: ����� �������� 
	int size;
	

public:
	short **pData;
	short maxRandomValue, minRandomValue;
	short const MAX_SIZE = 100;

	SquareMatrix(int newSize = 5);
	~SquareMatrix();

	int GetSize();
	int SetSize(int newSize);
	int SetRandomRange(int min, int max);

};

