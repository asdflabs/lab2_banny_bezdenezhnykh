#ifndef PCH_H
#define PCH_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include "SquareMatrix.h"
#include <string>
#include <iomanip>
#include <ctime>
#include <vector>

int StartMenu(SquareMatrix &testMatrix);
void DrawMenu();

int DisplayMatrixInConsole(SquareMatrix &workingMatrix);
int GetMinSumForColumn(SquareMatrix &workingMatrix, short &column);
std::string GetLocationOfMaxElement(SquareMatrix &workingMatrix);
int RegenerateMatrix(SquareMatrix &workingMatrix);
void ChangeMatrixRandomRange(SquareMatrix &workingMatrix);
void ChangeMatrixSize(SquareMatrix &workingMatrix);
int GetNumberOfNegativeStrings(SquareMatrix &workingMatrix);
int FillMatrixFromConsole(SquareMatrix &workingMatrix);

#endif //PCH_H
