#include "pch.h"

using namespace std;
const short NumberOfMenuItems = 9;

string MenuItems[NumberOfMenuItems]{
		"�������� �������",
		"���������������� �������",
		"�������� �������� ��������� �����",
		"�������� ������ �������",
		"����� ����������� ����� ��������� ��������",
		"����������� ������� ������� ������������� �������� �������",
		"���������� ���������� ����� ������� � �������������� ����������",
		"������ ������� �������",
		"����� �� ���������"
};

void DrawMenu() {
	system("CLS");;
	for (short i = 0; i < NumberOfMenuItems; i++)
	{
		cout << to_string(i + 1) << " ";
		cout << MenuItems[i] << endl;
	}
}

int StartMenu(SquareMatrix &testMatrix) {
	setlocale(0, "");
	DrawMenu();
	string input;
	short inputKey = 0;
	cout << ">> ";
	while (getline(cin, input), input != "9") {
		try {
			inputKey = stoi(input);
			inputKey--;
		}
		catch (exception) {
			continue;
		}

		switch (inputKey)
		{
		case 0: {
			DisplayMatrixInConsole(testMatrix);
			_getch();
			DrawMenu();
			break;
		}
		case 1: {
			RegenerateMatrix(testMatrix);
			DrawMenu();
			break;
		}
		case 2: {
			ChangeMatrixRandomRange(testMatrix);
			DrawMenu();
			break;
		}
		case 3: {
			ChangeMatrixSize(testMatrix);
			DrawMenu();
			break;
		}
		case 4: {
			short column;
			DisplayMatrixInConsole(testMatrix);
			cout << "����������� ����� �������: " << GetMinSumForColumn(testMatrix, column);
			printf(" (%d �������)\n", column);
			_getch();
			DrawMenu();
			break;
		}
		case 5: {
			DisplayMatrixInConsole(testMatrix);
			cout << endl;
			cout << GetLocationOfMaxElement(testMatrix);
			_getch();
			DrawMenu();
			break;
		}
		case 6: {
			DisplayMatrixInConsole(testMatrix);
			cout << "���������� ����� ������ ������������� �����: " << GetNumberOfNegativeStrings(testMatrix) << endl;
			_getch();
			DrawMenu();
			break;
		}
		case 7: {
			DisplayMatrixInConsole(testMatrix);
			cout << "---------------------------------------------------\n\n";
			FillMatrixFromConsole(testMatrix);
			_getch();
			DrawMenu();
			break;
		}
		case 8: {
			//������ �����, �� ����� �����, ������ �� � ���
			break;
		}

		default:
			break;
		}

		cout << ">> ";

	}

	return 0;
}