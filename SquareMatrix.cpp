#include "pch.h"
#include "SquareMatrix.h"

SquareMatrix::SquareMatrix(int size)
{
	this->size = size;
	maxRandomValue = 5;
	minRandomValue = -5;

	pData = new short*[MAX_SIZE];
	for (short i = 0; i < MAX_SIZE; i++)
	{
		pData[i] = new short[MAX_SIZE];
	}


}

SquareMatrix::~SquareMatrix()
{
	for (short i = 0; i < size - 1; i++)
	{
		delete[] pData[i];
	}
	delete[] pData;
}

int SquareMatrix::GetSize() {
	return size;
}

int SquareMatrix::SetSize(int newSize) {
	if (newSize <= 0 || newSize > MAX_SIZE) {
		return -1;
	}
	this->size = newSize;
	return 0;
}

int SquareMatrix::SetRandomRange(int min, int max)
{
	if (min > max) return -1;
	
	maxRandomValue = max;
	minRandomValue = min;
	return 0;
}



