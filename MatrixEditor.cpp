
#include "pch.h"


using namespace std;

int DisplayMatrixInConsole(SquareMatrix &workingMatrix) {
	system("CLS");
	cout << endl;
	for (short i = 0; i < workingMatrix.GetSize(); i++)
	{
		for (short j = 0; j < workingMatrix.GetSize(); j++)
		{
			cout << right << setw(3);
			cout << to_string(workingMatrix.pData[i][j]);
		}
		cout << endl;
	}
	return 0;
}

int GetMinSumForColumn(SquareMatrix &workingMatrix, short &column)
{
	int currentSum = 0;
	int minSum = 0;
	short minColumn = 1;
	for (int j = 0; j < workingMatrix.GetSize(); j++)
	{
		minSum += workingMatrix.pData[j][0];
	}
	for (int i = 0; i < workingMatrix.GetSize(); i++)
	{
		currentSum = 0;
		for (int j = 0; j < workingMatrix.GetSize(); j++)
		{
			currentSum += workingMatrix.pData[j][i];
		}
		if (minSum > currentSum)
		{
			minSum = currentSum;
			minColumn = i + 1;
		}
	}
	column = minColumn;
	return minSum;
}

string GetLocationOfMaxElement(SquareMatrix &workingMatrix)
{
	short maxElement = workingMatrix.pData[0][0];
	bool isBelow = false, isOn = false, isAbove = false;

	for (short i = 0; i < workingMatrix.GetSize(); i++)
	{
		for (short j = 0; j < workingMatrix.GetSize(); j++)
		{
			if (maxElement < workingMatrix.pData[i][j]) { maxElement = workingMatrix.pData[i][j]; }

		}
	}

	for (short i = 0; i < workingMatrix.GetSize(); i++)
	{
		for (short j = 0; j < workingMatrix.GetSize(); j++)
		{
			if (maxElement == workingMatrix.pData[i][j])
			{
				if (i + j + 1 == workingMatrix.pData[i][j]) isOn = true;
				if (i + j + 1 < workingMatrix.pData[i][j]) isAbove = true;
				else isBelow = true;
			}

		}
	}
	string result = (string)"������������ �������: " + to_string(maxElement) + (string)"\n���������:\n";
	if (isOn) result += "-�� �������� ���������\n";
	if (isAbove) result += "-��� �������� ����������\n";
	if (isBelow) result += "-��� �������� ����������\n";
	return result;

}

int RegenerateMatrix(SquareMatrix &workingMatrix) {
	srand(time(NULL));
	int positiveModifier = workingMatrix.maxRandomValue - workingMatrix.minRandomValue + 1;
	for (short i = 0; i < workingMatrix.GetSize(); i++)
	{
		for (short j = 0; j < workingMatrix.GetSize(); j++)
		{
			workingMatrix.pData[i][j] = rand() % positiveModifier + workingMatrix.minRandomValue;
		}

	}
	return 0;
}

void ChangeMatrixRandomRange(SquareMatrix &workingMatrix) {
	short min, max;
	try
	{
		cout << "������� ������ ������� ��������� �����: ";
		cin >> min;
		cout << "������� ������� ������� ��������� �����: ";
		cin >> max;
	}
	catch (const std::exception&)
	{
		cout << "��������, �� ����� �� �����, ������";
		_getch();
	}
	if (workingMatrix.SetRandomRange(min, max) == -1) { cout << "������ ���������.."; _getch(); }
	RegenerateMatrix(workingMatrix);
	cout << "������� ��������";
	_getch();
}

void ChangeMatrixSize(SquareMatrix &workingMatrix) {
	try
	{
		short size;
		cout << "������� ������ ������� - ����������� ����� �� ������ 100: ";
		cin >> size;
		if (workingMatrix.SetSize(size) == -1) cout << "�� ����� �������� ������!!!";
		else {
			RegenerateMatrix(workingMatrix);
			cout << "������� ��������!!!";
		}
		_getch();
	}
	catch (const std::exception&)
	{
		cout << "��������, �� ����� �� �����, ������";
		DrawMenu();
	}
	_getch();
}

int GetNumberOfNegativeStrings(SquareMatrix &workingMatrix) {
	int j;
	int count = 0;
	for (int i = 0; i < workingMatrix.GetSize(); i++)
	{
		for (j = 0; j < workingMatrix.GetSize(); j++)
		{
			if (workingMatrix.pData[i][j] >= 0)
			{
				break;
			}
		}
		if (j == workingMatrix.GetSize())
		{
			count++;
		}
	}
	return count;
}

vector<string> SplitString(string s, string delimiter) {
	vector<string> list;
	short start = 0, end = 0;
	string token;
	while ((end = s.find(delimiter, start)) != -1) {
		token = s.substr(start, end-start);
		if(token != "") list.push_back(token);
		start = end + delimiter.length();
	}
	list.push_back(s.substr(start));
	return list;
}

int FillMatrixFromConsole(SquareMatrix &workingMatrix) {
	string delimiter = " ";
	string userInput;
	vector<string> splittedString;
	cout << "������� ������� ���������: \n";
	for (short i = 0; i < workingMatrix.GetSize(); i++)
	{
		getline(cin, userInput);
		splittedString = SplitString(userInput, delimiter);
		if (splittedString.size() < workingMatrix.GetSize()) {
			cout << "�� ����� ������������� ���������� ����, ���������� �����: \n";
			i--;
			continue;
		}
		else {
			try {
				for (short j = 0; j < workingMatrix.GetSize(); j++)
				{
					workingMatrix.pData[i][j] = stoi(splittedString[j]);
				}
			}
			catch (exception) {
				cout << "�� ����� �� �����, ������� ������ ������:\n";
				i--;
				continue;
			}
		}
	}
	cout << "������� ������� ���������~\n";
	return 0;
}